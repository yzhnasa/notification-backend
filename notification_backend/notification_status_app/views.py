from django.shortcuts import render
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from notification_status_app.serializers import NotificationStatusCreateSerializer


@api_view(['POST', ])
@permission_classes((AllowAny, ))
@csrf_exempt
def create_notification_status_item(request, many='true'):
    if request.method != 'POST':
        return Response(status=status.HTTP_400_BAD_REQUEST)

    if many == 'true':
        serializer = NotificationStatusCreateSerializer(data=request.data, many=True)
    elif many == 'false':
        serializer = NotificationStatusCreateSerializer(data=request.data)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    if serializer.is_valid():
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
    else:
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

