from django.conf import settings
from django.db import models

class NotificationStatus(models.Model):
    NOTIFICATION = 'N'
    STATUS = 'S'

    TYPE = (
        (NOTIFICATION, 'notification'),
        (STATUS, 'status'),
    )
    user_hash_id = models.TextField(blank=False)
    time_stamp = models.DateTimeField(auto_now_add=True)
    action_type = models.CharField(max_length=10, blank=True)
    notification_id = models.IntegerField(null=True, blank=True)
    package_name = models.CharField(max_length=100, blank=True)
    type_flag = models.CharField(max_length=1, choices=TYPE, default=NOTIFICATION)
