from rest_framework import serializers
#from django.conf import settings
from notification_status_app.models import NotificationStatus


class NotificationStatusListCreateSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        bulk_notification_status = [NotificationStatus(**item) for item in validated_data]
        return NotificationStatus.objects.bulk_create(bulk_notification_status)


class NotificationStatusCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationStatus
        list_serializer_class = NotificationStatusListCreateSerializer
        fields = (
            'user_hash_id',
            'time_stamp',
            'action_type',
            'notification_id',
            'package_name',
            'type_flag',
        )

    def create(self, validated_data):
        notification_status = NotificationStatus(**validated_data)
        notification_status.save()
        return notification_status

